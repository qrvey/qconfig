var AWS = require('aws-sdk');
var validateSchemaData = require('./validate.js').validateSchemaData;
var schemaData = require('./QconfigSchema.json');
const Conf = require('conf');
const config = new Conf();

function QConfig(objectConfig) {
    if (objectConfig) {
        this.type = objectConfig.type;
        this.bucketName = objectConfig.bucketName;
        this.file = objectConfig.file;
        this.table = objectConfig.table;
        this.key = objectConfig.key;
        this.region = objectConfig.region;
        let type = this.type;
        validateSchemaData(schemaData, objectConfig).then(function () {
            if (type === 'S3') {
                var s3 = new AWS.S3();
                var params = {
                    Bucket: objectConfig.bucketName,
                    Key: objectConfig.file
                };

                s3.getObject(params, function (err, data) {
                    if (err) {
                        console.log(err);
                    } else {
                        config.set(JSON.parse(data.Body.toString('utf-8')));
                    }
                });
            } else {
                if (type === 'dynamodb') {
                    AWS.config.update({
                        region: objectConfig.region
                    });
                    var docClient = new AWS.DynamoDB.DocumentClient();

                    var params = {
                        TableName: objectConfig.table,
                        Key: {
                            "config": objectConfig.key
                        }
                    };
                    docClient.get(params).promise().then(function (data) {
                        if (data.Item != undefined) {
                            config.set(JSON.parse(data.Item.file));
                        }
                    });
                }
            }
        }, function (error) {
            throw new Error(error);
        });
    } else {
        throw new Error('objectConfig is required');
    }
}

QConfig.prototype.getValueKey = function (myKey) {
    return config.get(myKey);
};

var expo = {
    QConfig
};

module.exports = expo;