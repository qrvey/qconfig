'use strict';

var Validator = require('jsonschema').Validator;

exports.validateSchemaData = function (schemaData, validateData) {
    return new Promise(function (resolve, reject) {
        var v = new Validator();
        var result = v.validate(validateData, schemaData);
        if (result.valid) {
            resolve(true);
        } else {
            reject(result.errors);
            //throw new Error(result.errors);
        }
    });
};