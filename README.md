## Resumen

Package to manage the config files into the datarouter project or other projects.

## Getting Started

qrveyconfig is a package that allows you to manage the config files of the datarouter applications and other applications that are required, allowing you to store these configuration files in other sources such as AWS.S3 and AWS.DynamoDB.

## Installation

```sh
$ npm install --save @qrvey/qrveyconfig
```

## Usage

```js
var MyQConfig = require('@qrvey/qrveyconfig');
```

### AWS.S3 usage
If your config file is in AWS.S3 use the follow definition, using a object with three fields, "type": type of the config(S3 or DynamoDB), "bucketName": Name of the bucket where the file is upload and "file": Name of the file in the bucket in json format.
```js
var s3Config = {
    type: "S3",
    bucketName: "<bucketName>",
    file: "<fileName.json>"
};
var myConfig = new MyQConfig.qconfig(s3Config);
```

### AWS.DynamoDB usage
If your config file is in AWS.DynamoDB use the follow definition, using a object with four fields, "type": type of the config(S3 or DynamoDB), "table": Name of the table where the config is stored, "key" is the identification of the unique row where is the config. and "region": is the region of the dynamodb.

If your config file is in AWS.DynamoDB use the follow definition

```js
var dynamoDBConfig={
    type: "dynamodb",
    table: "<tableDynamoDB>",
    key: "<keyRow>",
    region: "<region>"
};
var myConfig = new MyQConfig.qconfig(dynamoDBConfig);
```
### Get key

To return values from the config file you must use the 'getValueKey' method.

```js
var myValue = myConfig.getValueKey('myKey');
```